FROM registry.gitlab.com/wernerraath/geopython_docker:master

ADD DJANGO_REQUIREMENTS.txt /runtime/

RUN pip install --upgrade pip \
    && pip install -r /runtime/DJANGO_REQUIREMENTS.txt -v \
    && python -c "from django.contrib.gis.geos import GEOSGeometry; print(GEOSGeometry('POINT(0 0)'))"

WORKDIR /django_project/
